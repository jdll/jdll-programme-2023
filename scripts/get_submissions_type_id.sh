#! /usr/bin/env bash

DEPENDENCIES=("curl" "jq")

for str in "${DEPENDENCIES[@]}"; do
    if ! command -v "$str" &> /dev/null; then
      echo "ERROR: $str is missing. Please install $str."
      exit 1
    fi
done

source .env

DATA=$(curl -H "Authorization: Token ${PRETALX_API_KEY}" \
    "https://$PRETALX_HOSTNAME/api/events/$PRETALX_EVENT_ID/submissions/" \
    2> /dev/null)

TYPE=$(echo "$DATA" \
    | jq '.results[] | [.submission_type_id, .submission_type.fr]')

echo "Event: $PRETALX_EVENT_ID"
echo "$TYPE" | tr ",\\n" " " | tr [ \\n | cut -d"]" -f1 | sort | uniq
