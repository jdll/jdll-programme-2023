#!/usr/bin/bash

gs -o 1.pdf -sDEVICE=pdfwrite -dFirstPage=1 -dLastPage=1 -dDEVICEWIDTHPOINTS=276 -dDEVICEHEIGHTPOINTS=392 -dFIXEDMEDIA -c "<</PageOffset [-176 -216]>> setpagedevice" -f Journées\ du\ Logiciel\ Libre\ 2023_V6.pdf 

gs -o 2-40.pdf -sDEVICE=pdfwrite -dFirstPage=2 -dLastPage=40 -dDEVICEWIDTHPOINTS=276 -dDEVICEHEIGHTPOINTS=392 -dFIXEDMEDIA -c "<</PageOffset [-176 -250]>> setpagedevice" -f Journées\ du\ Logiciel\ Libre\ 2023_V6.pdf

gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=jdlldef.pdf 1.pdf 2-40.pdf 
