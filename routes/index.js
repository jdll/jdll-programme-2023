const express = require("express");
const router = express.Router();
const internalDataModel = require("../middlewares/internalDataModel");

/* GET home page. */
router.get("/", async (req, res) => {
  let data = await internalDataModel();
  //console.log(data);
  res.render("index", { data: data });
});

module.exports = router;
