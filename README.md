# Programme JdLL 2023

## Génération du Programme Papier/pdf des JdLL 2023

Le programme des [JdLL](https://www.jdll.org/) est généré de manière
automatisée.
Les données son récupérées via l'API du logiciel
[pretalx](https://pretalx.com/p/about/) qui est le système d'information
des JdLL.
La répartition sous forme de pages, la mise au format épreuves d'impression
ainsi que l'export en pdf sont faites par la librairie
[bindery.js](https://bindery.info/).
Le templating est fait en [ejs](https://ejs.co/).
Le tout est mis dans un serveur [expressJs](https://expressjs.com/) qui sert
d'environnement de travail local.

Ce projet est le prolongement de celui initié par nos prédécesseurs
[pour le programme 2022](https://gitlab.com/jdll/jdll-programme-2022).

## Préparer son environnement

Tout d'abord il faut cloner ce projet

```bash
git clone https://gitlab.com/jdll/jdll-programme-2023
```

Puis installer les dépendances `node`.

```bash
npm install
```

Enfin créer un fichier nommé `.env` à la racine du projet.
Il faut le remplir avec les champs suivants

```ini
PRETALX_HOSTNAME={pretalx.jdll.org}
PRETALX_API_KEY={votre_clé_API}
PRETALX_EVENT_ID={id_de_l_event}
CONF_ID={id_du_type_de_soumissions_conférences}
ATELIER_ID={id_du_type_de_soumissions_ateliers}
GONE_ID={id_du_type_de_soumissions_pole_gone}
STAND_ID={id_de_type_de_soumissions_stands}
Q_BEGINNER_ID={id_de_la_question_débutant_ou_confirmé}
Q_PUBLICS_ID={id_de_la_question_sur_les_publics_concernés}
```

Pour récupérer les ids des types de soumissions, vous pouvez lancez le script
`get_submissions_typeid.sh` depuis la racine du projet :

```shell
scripts/get_subbmissions_type_id.sh
```

## Fonctionnement

Lancer l'application par

```bash
npm start
```

Lorsque l'on se rend sur `localhost:3000` dans un navigateur internet :

- Les données brutes sont récupérées depuis le pretalx et
  reformatées/simplifiées pour être plus proches de la structure du programme.
  `route/index.js -> middlewares/pretalxHandler.js`
- Celle-ci sont passées au processeur ejs qui distribue ces données dans les
  templates appropriés, cela produit une page html linéaire qui contient tout
  le programme sous forme d'une simple page html continue. `views`
- Bindery.js fait son office et ajoute les transformations propres au "print".
  Mises en forme conditionnelles en fonction des pages droites ou gauche
  et découpage par page. `views/index.ejs -> balise <script> au bas du fichier`

## Modèle de données interne

### Modèle général

```json
{
    "event": {
        "name": 'Journées du Logiciel Libre 2023',
        "date": {
            "start": 2022-04-01T00:00:00.000Z,
            "end": 2022-04-02T00:00:00.000Z
        }
    },
    "samedi": {
        "confs": [<talks>],
        "ateliers": [<talks>],
        "gone": [<talks>]
    },
    "dimanche": {
        "confs": [<talks>],
        "ateliers": [<talks>],
        "gone": [<talks>]
    },
    "stands": [<stands>]
}
```

### Modèle des talks

```json
{
  "type": "Conférence",
  "typeId": 29,
  "speakers": "Noms, des, conférenciers",
  "beginner": true,
  "publics": "Associatif",
  "room": "Nom de la salle",
  "startTime": "2022-04-03T17:00:00+02:00",
  "duration": 55,
  "title": "Titre de la conférence",
  "abstract": "Résumé du propos de la conférence."
}
```

### Modèle des stands

Le modèle des stands reprend le modèle des talks et lui adjoint une clé
supplémentaire `standNumber`.

## Génération

Chromium est le navigateur à privilégier pour ne pas avoir de problème
d'export.
Le niveau de zoom change l'export.

- Fait sur un chromium Version 110.0.5481.100
  (Build officiel) Arch Linux (64 bits)
- Zoom 150%
- print preview > 1 page /sheet > A4 portrait > crop & bleed
- Puis recadré en A6 via imageMagick pour toutes les pages sauf la première.

```bash
convert -density 288 JdLL2023_ProgrammeA6inA4.pdf[1-39] \
-crop 1190x1678+664+744 +repage JdLL2023_Programme_temp.pdf
```

Il faut recadrer la couverture séparément car un bug la positionne mal
à la génération.

```bash
convert -density 288 JdLL2023_ProgrammeA6inA4.pdf[0] \
-crop 1190x1678+664+886 +repage JdLL2023_Programme_couv.pdf
```

Puis il faut substituer la première page par la nouvelle dans le pdf.

```bash
convert -density 288 \
JdLL2023_Programme_couv.pdf \
JdLL2023_Programme_temp.pdf \
JdLL2023_Programme_def.pdf
```
