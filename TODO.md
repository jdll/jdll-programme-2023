A faire :

## Gestion des données

- [ ] Récupérations de l'information de parcours
- [ ] Récupération des données des stands directement sur le pretalx, avec : nom, numéro de table et résumé (et parcours ?)
      avec nettoyage des textes comme pour les confs
- [ ] prise en compte formatage MD (voir stands 6, 37)
- [ ] Affichage : rendre impossible le report du contenu de stand d'une colonne à une autre (voir 26, 39)

## Contenus

Gérer le nombre de page du doc, pour avoir les deux pages couleurs au centre du livret

- [ ] Retravailler le plan du Gymnase
- [ ] QR du wifi a générer avec les vraies infos (qrencode -t SVG -l H -o "qr_wifi.svg" "WIFI:T:WPA;S:<SSID>;P:<PSWD>;;")
- [ ] des textes d'intro pour les confs ? les ateliers ? les trucs de gônes ?
- [ ] par du texte, présenter les parcours ? Une page/paragraphe par parcours ?
- [ ] Travailler les contenus textuels

## Présentation

- [ ] en couleur partout ? Quel différence de prix ?
- [ ] CSS : mise en évidence des items avec parcours
- [ ] CSS : si couleur partout, mettre plus en évidence les titres de sections (confs, ateliers, etc.)

## En marge

- [ ] Si validé (voir page jdll): créer une URL jdll.org/donner qui pointerait vers la page hello-asso qui va bien
